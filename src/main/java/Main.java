import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import static java.lang.System.out;

public class Main {
    public final static long EVERY_MINUTE = 60000;
    public final static long EVERY_HALF_MINUTE = 30000;
    public final static long EVERY_QUARTER_MINUTE = 20000;
    public final static String key = "UUZKVYEH-7LP65S7Y-7DFOZVPM-IUOEEQRY";
    public final static String secret = "982f53bef8d252f0d0f659d06bf0b05578c34bc964194ae43fe40b5408b997a5dd75fb359fec5b1b087821cb7ad9fe6a28774b10a47d94f3b08a9af0221107be";
    public final static String url = "https://poloniex.com/tradingApi";
    public static void main(String args[]) throws InterruptedException {
        while (true) {
            printBalance();
            try {
                Class.forName("org.postgresql.Driver");
            } catch (Exception e) {
                out.println(e.getMessage());
            }
            try {
                Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DB?user=imwas&password=2151997mohm");
                PreparedStatement ps = conn.prepareStatement("CREATE TABLE IF NOT EXISTS pair(id SERIAL NOT NULL PRIMARY KEY," +
                        "pair_name text NOT NULL," +
                        "pair_id numeric  NOT NULL ," +
                        "last numeric  NOT NULL ," +
                        "lowestAsk numeric NOT NULL ," +
                        "highestBid numeric NOT NULL ," +
                        "percentChange numeric NOT NULL ," +
                        "baseVolume numeric NOT NULL ," +
                        "isFrozen text NOT NULL ," +
                        "high24hr numeric NOT NULL ," +
                        "low24hr numeric NOT NULL)");
                ps.executeUpdate();
                ps.close();
                String Tiker = getTiker();
                for (int i = 0; i < Names.CurrenciesPairsName.length; i++) {
                    PreparedStatement ps2 = conn.prepareStatement(getQury(Tiker, Names.CurrenciesPairsName[i]));
                    ps2.executeUpdate();
                    ps2.close();
                }
            } catch (Exception e) {
                out.println(e.getMessage());
            }
            String f = getTiker();
            for (int i = 0; i < Names.CurrenciesPairsName.length; i++) {
                CurrenciesPairs currenciesPairs = new CurrenciesPairs(f, Names.CurrenciesPairsName[i]);
                if (Double.parseDouble(currenciesPairs.getPercentChange().toString()) > 3) {
                    out.println(Names.CurrenciesPairsName[i] + "has changes " + currenciesPairs.getPercentChange());
                }
            }
            out.println("*******************************************************************************************************");
            Thread.sleep(10);
        }
    }
    public static String getTiker(){
        API api1 = new API("https://poloniex.com/public?command=returnTicker");
        try {
            return api1.getCurrenciesPairs();
        }catch (IOException e){
            out.println(e.getMessage());
        }
        return null;
    }
    public static  String getBalance(){
        API api = new API(key,secret,url);
        try{
           return api.getBalances();
        }catch (NoSuchAlgorithmException e){
            out.println(e.getMessage());
        }catch (InvalidKeyException e){
            out.println(e.getMessage());
        }catch (IOException e){
            out.println(e.getMessage());
        }
        return null;
    }
    public static String getQury(String data , String name){
        CurrenciesPairs currenciesPairs = new CurrenciesPairs(data,name);
        String Qury = "VALUES ("+"\'"+name+"\'"+","+currenciesPairs.getPairId()+","
                +currenciesPairs.getLast()+","+
                currenciesPairs.getLowestAsk()+
                ","+currenciesPairs.getHighestBid()
                +","+currenciesPairs.getPercentChange()+","+currenciesPairs.getBaseVolume()
                +","+currenciesPairs.isFrozen()+","
                +currenciesPairs.getHigh24hr()+","+currenciesPairs.getLow24hr()+")";
        Qury = "INSERT INTO pair(pair_name, pair_id, last, lowestask, highestbid, percentchange, basevolume, isfrozen, high24hr, low24hr)"+
                Qury;
        return Qury;
    }
    public static void printBalance(){
        String balance = getBalance();
        JsonParser parser = new JsonParser();
        JsonObject object = (JsonObject)  parser.parse(balance);
        for(int i=0;i<Names.CurrenciesName.length;i++){
            out.println(Names.CurrenciesName[i] + " = " + object.get(Names.CurrenciesName[i]));
        }
    }
}
