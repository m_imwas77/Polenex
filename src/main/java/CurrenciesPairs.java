import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigDecimal;

public class CurrenciesPairs {
    private  String data;
    private  String pairName;
    CurrenciesPairs(){}

    CurrenciesPairs(String data,String pairName){
        setData(data);
        setPairName(pairName);
    }

    public void setData(String data){
        this.data = data;
    }

    public final  String getData(){
        return this.data;
    }

    public void setPairName(String pairName){
        this.pairName = pairName;
    }

    public final  String getPairName(){
        return this.pairName;
    }

    private JsonObject getJsonObject(){
        JsonParser parser = new JsonParser();
        JsonObject objects = (JsonObject) parser.parse(this.getData());
        JsonObject object = (JsonObject) objects.get(this.getPairName());
        return  object;
    }

    public long  getPairId(){
        JsonElement object =  this.getJsonObject().get("id");
        return object.getAsLong();
    }

    public  BigDecimal getLast(){
        JsonElement object = this.getJsonObject().get("last");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getLowestAsk(){
        JsonElement object = this.getJsonObject().get("lowestAsk");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getHighestBid(){
        JsonElement object = this.getJsonObject().get("highestBid");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getPercentChange	(){
        JsonElement object = this.getJsonObject().get("percentChange");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal  getBaseVolume(){
        JsonElement object = this.getJsonObject().get("baseVolume");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public BigDecimal getQuoteVolume(){
        JsonElement object = this.getJsonObject().get("quoteVolume");
        return object.getAsBigDecimal();
    }

    public  BigDecimal getHigh24hr(){
        JsonElement object = this.getJsonObject().get("high24hr");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public  BigDecimal getLow24hr(){
        JsonElement object = this.getJsonObject().get("low24hr");
        return (BigDecimal) object.getAsBigDecimal();
    }

    public boolean isFrozen(){
        JsonElement object = this.getJsonObject().get("isFrozen");
        return (boolean) object.getAsBoolean();
    }

}
